<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-body">
            <!-- start stripe setting -->

            <section id="basic-vertical-layouts">
                <div class="row match-height justify-content-center">
                    <div class="col-md-8 col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Xendit Settings</h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <?= form_open_multipart('settings/editstripe'); ?>
                                    <form class="form form-vertical">
                                        <div class="form-body">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <label for="stripesecretkey">Xendit Api Key</label>
                                                        <input type="text" class="form-control" id="stripesecretkey" name="xendit_api_key" value="<?= $appsettings['xendit_api_key']; ?>" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="ilumaapikey">Iluma Api Key</label>
                                                        <input type="text" class="form-control" id="ilumaapikey" name="iluma_api_key" value="<?= $appsettings['iluma_api_key']; ?>" required>
                                                    </div>

                                                    <div class="col-12">
                                                        <button type="submit" class="btn btn-primary mr-1 mb-1">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                    </form>
                                    <?= form_close(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- end of stripe setting data -->
        </div>
    </div>
</div>
<!-- END: Content-->