<?php


class Notification_model extends CI_model
{
    public function notif_cancel_user($driver_id, $transaction_id, $token_user)
    {
        $CI = &get_instance();
        $CI->load->config('fcm');

        $fcmKey = $CI->config->item('fcm_key');
        $fcmUrl = $CI->config->item('fcm_url');

        $datanotif = array(
            'driver_id' => $driver_id,
            'transaction_id' => $transaction_id,
            'response' => '5',
            'type' => 1
        );
        $senderdata = array(
            'data' => $datanotif,
            'to' => $token_user
        );
        $headers = array(
            "Content-Type: application/json",
            "Authorization: key=" .$fcmKey
        );
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $fcmUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($senderdata),
            CURLOPT_HTTPHEADER => $headers,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        return $response;
    }

    public function notif_cancel_driver($transaction_id, $token_driver)
    {
        $CI = &get_instance();
        $CI->load->config('fcm');

        $fcmKey = $CI->config->item('fcm_key');
        $fcmUrl = $CI->config->item('fcm_url');

        $data = array(
            'transaction_id' => $transaction_id,
            'response' => '0',
            'type' => 1
        );
        $senderdata = array(
            'data' => $data,
            'to' => $token_driver
        );

        $headers = array(
            "Content-Type: application/json",
            'Authorization: key=' .$fcmKey
        );
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $fcmUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($senderdata),
            CURLOPT_HTTPHEADER => $headers,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        return $response;
    }

    public function send_notif($title, $message, $topic)
    {
        $CI = &get_instance();
        $CI->load->config('fcm');

        $fcmKey = $CI->config->item('fcm_key');
        $fcmUrl = $CI->config->item('fcm_url');

        $data = array(
            'title' => $title,
            'message' => $message,
            'type' => 4
        );
        $senderdata = array(
            'data' => $data,
            'to' => '/topics/' .$topic
        );

        $headers = array(
            "Authorization: key=" .$fcmKey,
            "Content-Type: application/json"
        );
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $fcmUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($senderdata),
            CURLOPT_HTTPHEADER => $headers,
        ));

        $response = curl_exec($curl);
        if ($response) {
            $data = array(
                'id' => floor(microtime(true) * 1000),
                'title' => $title,
                'message' => $message,
                'topic' => $topic,
            );
            $this->addNotif($data);
        }

        curl_close($curl);
        return $response;
    }

    public function send_notif_topup($data, $token)
    {
        $CI = &get_instance();
        $CI->load->config('fcm');

        $fcmKey = $CI->config->item('fcm_key');
        $fcmUrl = $CI->config->item('fcm_url');

        $senderdata = array(
            'data' => $data,
            'to' => $token

        );

        $headers = array(
            'Content-Type: application/json',
            'Authorization: key=' .$fcmKey
        );
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $fcmUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($senderdata),
            CURLOPT_HTTPHEADER => $headers,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        return $response;
    }

    public function send_notif_wd($title, $message, $token)
    {
        $CI = &get_instance();
        $CI->load->config('fcm');

        $fcmKey = $CI->config->item('fcm_key');
        $fcmUrl = $CI->config->item('fcm_url');

        $data = array(
            'title' => $title,
            'message' => $message,
            'type' => 7
        );

        $senderdata = array(
            'data' => $data,
            'to' => $token

        );

        $headers = array(
            'Content-Type: application/json',
            'Authorization: key=' .$fcmKey
        );
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $fcmUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($senderdata),
            CURLOPT_HTTPHEADER => $headers,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        return $response;
    }

    public function send_mobile_notif($data, $token)
    {
        $CI = &get_instance();
        $CI->load->config('fcm');

        $fcmKey = $CI->config->item('fcm_key');
        $fcmUrl = $CI->config->item('fcm_url');

        $senderdata = array(
            'data' => $data,
            'to' => $token
        );

        $headers = array(
            "Authorization: key=" .$fcmKey,
            "Content-Type: application/json"
        );
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $fcmUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($senderdata),
            CURLOPT_HTTPHEADER => $headers,
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }

    public function addNotif($data) {
        return $this->db->insert('admin_notification', $data);
    }
}
