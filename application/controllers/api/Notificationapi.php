<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class Notificationapi extends REST_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Notification_model', 'notif');
    }

    function sendmobilenotification_post() {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $decoded_data = json_decode($data);

        $send = $this->notif->send_mobile_notif($decoded_data->data, $decoded_data->token);

        if ($send) {
            $message = array(
                'code' => '200',
                'message' => 'success',
            );
        } else {
            $message = array(
                'code' => '404',
                'message' => 'failed',
            );
        }
        $this->response($message, 200);
    }
}