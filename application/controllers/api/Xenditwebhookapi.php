<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class XenditWebhookapi extends REST_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('Customer_model');
		$this->load->model('Driver_model');
		$this->load->model('Notification_model', 'notif');
		$this->load->model('Wallet_model', 'wlt');
		$this->load->helper('url');
		date_default_timezone_set(time_zone);
	}

	public function payment_notification_post()
	{
		$xenditCallbackToken = 'civzUWYJNTs1hHaaNREymiq4srctvUteqRLcyLtGx8QXR6zZ';
		$xIncomingCallbackTokenHeader = $this->input->get_request_header('x-callback-token', TRUE);

		if ($xIncomingCallbackTokenHeader === $xenditCallbackToken) {
			$rawRequestInput = file_get_contents("php://input");
			$request = json_decode($rawRequestInput, true);

			$reference_id = $request['data']['payment_method']['reference_id'];
			$date = $request['data']['payment_method']['updated'];
			$type = $request['data']['payment_method']['type'];

			$trans = $this->Customer_model->gettransaction($reference_id);
			$row = $trans->row_array();
			$userId = $row['user_id'];
			$amount = $row['amount'];
			$name = $row['name'];
			$type_user = $row['type_user'];

			$datatopup = array(
				'id_user' => $userId,
				'wallet_account' => '',
				'bank' => $type,
				'holder_name' => $name,
				'type' => 'topup',
				'wallet_amount' => $amount,
				'status' => 1
			);

			$data = array(
				'title' => 'Isi Ulang',
				'message' => 'Pembayaran anda berhasil ' . $amount,
				'date' => $date,
				'amount' => $amount,
				'payment_code' => $type,
				'reference_id' => $reference_id,
				'type' => 5
			);

			$token = '';

			if ($type_user == 'customer') {
				$result = $this->wlt->gettoken($userId);
				$token = $result['token'];
			} else if ($type_user == 'driver') {
				$result = $this->wlt->getregid($userId);
				$token = $result['reg_id'];
			} else if ($type_user == 'merchant') {
				$result = $this->wlt->gettokenmerchant($userId);
				$token = $result['merchant_token'];
			}

			$this->Customer_model->insertwallet($datatopup);
			$saldolama = $this->Customer_model->saldouser($userId);
			$saldobaru = $saldolama->row('balance') + $amount;
			$balance = array('balance' => $saldobaru);
			$this->Customer_model->addsaldo($userId, $balance);


			$this->notif->send_notif_topup($data, $token);

		} else {
			http_response_code(403);
		}
	}

	public function payment_notification_failed_post()
	{
		$xenditCallbackToken = 'civzUWYJNTs1hHaaNREymiq4srctvUteqRLcyLtGx8QXR6zZ';
		$xIncomingCallbackTokenHeader = $this->input->get_request_header('x-callback-token', TRUE);

		if ($xIncomingCallbackTokenHeader === $xenditCallbackToken) {
			$rawRequestInput = file_get_contents("php://input");
			$request = json_decode($rawRequestInput, true);

			$reference_id = $request['data']['payment_method']['reference_id'];
			$channel_code = $request['data']['payment_method']['ewallet']['channel_code'];

			$trans = $this->Customer_model->gettransaction($reference_id);
			$row = $trans->row_array();
			$userId = $row['user_id'];
			$type_user = $row['type_user'];

			$data = array(
				'type' => 6
			);

			$token = '';

			if ($type_user == 'customer') {
				$result = $this->wlt->gettoken($userId);
				$token = $result['token'];
			} else if ($type_user == 'driver') {
				$result = $this->wlt->getregid($userId);
				$token = $result['reg_id'];
			} else if ($type_user == 'merchant') {
				$result = $this->wlt->gettokenmerchant($userId);
				$token = $result['merchant_token'];
			}

			if ($channel_code == 'OVO') {
				$this->notif->send_notif_topup($data, $token);
			}

		} else {
			http_response_code(403);
		}
	}

	public function disbursement_notification_post()
	{
		$xenditCallbackToken = 'civzUWYJNTs1hHaaNREymiq4srctvUteqRLcyLtGx8QXR6zZ';
		$xIncomingCallbackTokenHeader = $this->input->get_request_header('x-callback-token', TRUE);

		if ($xIncomingCallbackTokenHeader === $xenditCallbackToken) {
			$rawRequestInput = file_get_contents("php://input");
			$request = json_decode($rawRequestInput, true);

			$external_id = $request['external_id'];
			$amount = $request['amount'];
			$status_disburse = $request['status'];

			$status = 0;

			if ($status_disburse == "COMPLETED") {
				$status = 1;
			} else if ($status_disburse == "FAILED") {
				$status = 2;
			}


			$trans = $this->Customer_model->gettransaction($external_id);
			$row = $trans->row_array();
			$userId = $row['user_id'];
			$type_user = $row['type_user'];
			$amountWithAdmin = $row['amount'];

			$token = '';

			if ($type_user == 'customer') {
				$result = $this->wlt->gettoken($userId);
				$token = $result['token'];
			} else if ($type_user == 'driver') {
				$result = $this->wlt->getregid($userId);
				$token = $result['reg_id'];
			} else if ($type_user == 'merchant') {
				$result = $this->wlt->gettokenmerchant($userId);
				$token = $result['merchant_token'];
			}

			$this->wlt->updatestatuswithdrawbyid($external_id, $status);

			if ($status == 1) {

				$title = 'Penarikan';
				$message = 'Penarikan anda berhasil ' . $amount;
				$this->notif->send_notif_wd($title, $message, $token);

			} else if ($status == 2) {

				$saldolama = $this->Customer_model->saldouser($userId);
				$saldobaru = $saldolama->row('balance') + $amountWithAdmin;
				$balance = array('balance' => $saldobaru);
				$this->Customer_model->addsaldo($userId, $balance);

				$title = 'Penarikan';
				$message = 'Penarikan Gagal, Silakan Hubungi Customer Service Kami ';
				$this->notif->send_notif_wd($title, $message, $token);
			}
		} else {
			http_response_code(403);
		}
	}
}
